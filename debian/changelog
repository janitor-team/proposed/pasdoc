pasdoc (0.15.0-3) UNRELEASED; urgency=low

  * Set upstream metadata fields: Archive, Repository.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 19 Jul 2020 15:40:03 -0000

pasdoc (0.15.0-2) unstable; urgency=medium

  * Use dh_install instead of dh_installman as help2man is broken (Closes:
    #954676)
  * Switch to compat 13 (via debhelper-compat B-D)
  * Bump standards to 4.5.0
  * Fix Vcs-Browser field
  * Add check for DEB_BUILD_OPTIONS in dh_auto_test override
  * Add R³: no

 -- Paul Gevers <elbrus@debian.org>  Mon, 20 Apr 2020 21:18:23 +0200

pasdoc (0.15.0-1) unstable; urgency=medium

  * New upstream release
    - CVE-2017-17527 Upstream removed the relevant code as it wasn't used
      anyways, see
      https://lists.alioth.debian.org/pipermail/pkg-pascal-devel/Week-of-Mon-20171211/002156.html
    - Adapt packaging in multiple ways to new source
  * Use system version of jquery during build (Closes: #858640)
  * Drop user documentation as upstream doesn't ship it anymore

 -- Paul Gevers <elbrus@debian.org>  Fri, 09 Feb 2018 22:42:15 +0100

pasdoc (0.14.0-1) unstable; urgency=medium

  * New upstream release (the reproducicble build release)
  * Update copyright information
  * Bump standards-version (no changes)
  * Drop obsolete patch (applied upstream)
  * Drop debian/missing-sources as it is now upstream

 -- Paul Gevers <elbrus@debian.org>  Sun, 09 Aug 2015 20:07:06 +0200

pasdoc (0.13.0-1) unstable; urgency=medium

  * Initial release. (Closes: #747547)

 -- Paul Gevers <elbrus@debian.org>  Sat, 17 May 2014 12:26:42 +0200
