{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit pasdoc_package;

{$warn 5023 off : no warning about unused units}
interface

uses
  PasDoc_Gen, PasDoc_GenHtml, PasDoc_GenHtmlHelp, PasDoc_GenLatex, 
  PasDoc_GenSimpleXML, PasDoc_Hashes, PasDoc_HierarchyTree, PasDoc_Items, 
  PasDoc_Languages, PasDoc_ObjectVector, PasDoc_OptionParser, PasDoc_Parser, 
  PasDoc_Reg, PasDoc_Scanner, PasDoc_Serialize, PasDoc_SortSettings, 
  PasDoc_StreamUtils, PasDoc_StringVector, PasDoc_TagManager, 
  PasDoc_Tokenizer, PasDoc_Types, PasDoc_Utils, PasDoc_Versions, PasDoc_Tipue, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('PasDoc_Reg', @PasDoc_Reg.Register);
end;

initialization
  RegisterPackage('pasdoc_package', @Register);
end.
